# fastapi server

    pipenv run uvicorn main:app --reload

## Tutorials

- [vue3 fast api](https://testdriven.io/blog/developing-a-single-page-app-with-fastapi-and-vuejs/#fastapi-setup)
- [async fast api with postgresql and migrations with alembic](https://testdriven.io/blog/fastapi-sqlmodel/)

## Passage par Docker
