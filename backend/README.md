# Backend fastapi sous Docker

sudo docker build -t fastapitest .

sudo docker run -d --name apitest --rm -p 8000:8000 fastapitest

On peut trouver l'adresse IP de la machine virtuelle avec la commande `sudo docker inspect 4e1d72ec4496`.
