from typing import Optional

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from sqlmodel import Field, Session, SQLModel, create_engine, select
from pydantic import HttpUrl
from fastapi.middleware.cors import CORSMiddleware


class Score(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    url: HttpUrl
    name: str = Field(index=True)
    score: int


sqlite_file_name = "database.db"
sqlite_url = f"sqlite:///{sqlite_file_name}"

connect_args = {"check_same_thread": False}
engine = create_engine(sqlite_url, echo=True, connect_args=connect_args)


def create_db_and_tables():
    SQLModel.metadata.create_all(engine)


app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["https://benabel.frama.io", "https://82.66.190.232:5173", "*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event("startup")
def on_startup():
    create_db_and_tables()


@app.get("/")
def index():
    return {"msg": "Heyyy"}


@app.post("/scores/")
def record_score(score: Score):
    with Session(engine) as session:
        session.add(score)
        session.commit()
        session.refresh(score)
        return score


@app.get("/scores/")
def read_scores():
    with Session(engine) as session:
        scores = session.exec(select(Score)).all()
        return scores
